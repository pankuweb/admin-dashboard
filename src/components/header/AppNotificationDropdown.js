import React from 'react'
import {
  CDropdown,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'
import { cilUser, cilBell } from '@coreui/icons'
import CIcon from '@coreui/icons-react'

const AppNotificationDropdown = () => {
  return (
    <CDropdown variant="nav-item">
      <CDropdownToggle placement="bottom-end" className="p-0" caret={false}>
        <CIcon icon={cilBell} size="lg" />
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownHeader className="bg-light fw-semibold py-2">Notifications</CDropdownHeader>{' '}
        <CDropdownItem href="#">
          <CIcon icon={cilUser} className="me-2 text-success" />
          New User Added
        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon icon={cilUser} className="me-2 text-success" />
          User Added
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default AppNotificationDropdown
