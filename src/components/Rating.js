import React from "react";
import ReactStars from "react-rating-stars-component";

import { useState } from "react";
const Rating = () => {
  const [stars, setStars] = useState(4);

  var example = {
    size: 16,
    value: stars,
    onChange: (newValue) => {
      setStars(newValue);
    },
  };

  console.log(example.value);

  return (
    <div className="App">
      {/* <button onClick={() => setStars(5)}>Set To 5</button> */}
      <ReactStars {...example} />
    </div>
  );
};

export default Rating;
