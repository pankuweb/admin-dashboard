import React from 'react'

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))

// Movies
const MovieList = React.lazy(() => import('./views/movies/movie-list/MovieList'))
const AddMovie = React.lazy(() => import('./views/movies/add-movie/AddMovie'))
const EditMovie = React.lazy(() => import('./views/movies/edit-movie/EditMovie'))
const MovieDetail = React.lazy(() => import('./views/movies/movie-detail/movieDetail'))

// Users
const UserList = React.lazy(() => import('./views/users/user-list/UserList'))
const AddUser = React.lazy(() => import('./views/users/add-user/AddUser'))
const EditUser = React.lazy(() => import('./views/users/edit-user/EditUser'))
const UserDetail = React.lazy(() => import('./views/users/user-detail/userDetail'))

//category
const AddCategory = React.lazy(() => import('./views/category/add-category/AddCategory'))
const EditCategory = React.lazy(() => import('./views/category/edit-category/EditCategory'))
const CategoryList = React.lazy(() => import('./views/category/category-list/CategoryList'))

// Coupons
const CouponList = React.lazy(() => import('./views/coupons/coupon-list/CouponList'))
const AddCoupon = React.lazy(() => import('./views/coupons/add-coupon/AddCoupon'))
const EditCoupon = React.lazy(() => import('./views/coupons/edit-coupon/EditCoupon'))

// Reviews
const ReviewList = React.lazy(() => import('./views/reviews/review-list/ReviewList'))
const EditReview = React.lazy(() => import('./views/reviews/edit-review/EditReview'))

// Casts
const CastList = React.lazy(() => import('./views/casts/cast-list/CastList'))
const AddCast = React.lazy(() => import('./views/casts/add-cast/AddCast'))
const EditCast = React.lazy(() => import('./views/casts/edit-cast/EditCast'))
const CastDetail = React.lazy(() => import('./views/casts/cast-detail/CastDetail'))

// Languages
const LanguageList = React.lazy(() => import('./views/languages/language-list/LanguageList'))
const AddLanguage = React.lazy(() => import('./views/languages/add-language/AddLanguage'))
const EditLanguage = React.lazy(() => import('./views/languages/edit-language/EditLanguage'))

// Profile
const Profile = React.lazy(() => import('./views/profile/Profile'))

const routes = [
  { path: '/', name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/movies', name: 'Movies', component: MovieList, exact: true },
  { path: '/movies/movie-list', name: 'All Movies', component: MovieList },
  { path: '/movies/add-movie', name: 'Add New Movie', component: AddMovie },
  { path: '/movies/edit-movie', name: 'Edit Movie', component: EditMovie },
  { path: '/movies/movie-detail', name: 'Movie Detail', component: MovieDetail },
  { path: '/users', name: 'Users', component: UserList, exact: true },
  { path: '/users/user-list', name: 'Users List', component: UserList },
  { path: '/users/add-user', name: 'Add User', component: AddUser },
  { path: '/users/edit-user', name: 'Edit User', component: EditUser },
  { path: '/users/user-detail', name: 'User Detail', component: UserDetail },
  { path: '/category', name: 'Categories', component: AddCategory, exact: true },
  { path: '/category/add-category', name: 'Add Category', component: AddCategory },
  { path: '/category/edit-category', name: 'Edit Category', component: EditCategory },
  { path: '/category/category-list', name: 'Category List', component: CategoryList },
  { path: '/coupons', exact: true, name: 'Coupons', component: AddCoupon },
  { path: '/reviews', exact: true, name: 'Reviews', component: ReviewList },
  { path: '/reviews/review-list', name: 'Review List', component: ReviewList },
  { path: '/reviews/edit-review', name: 'Edit Review', component: EditReview },
  { path: '/coupons/add-coupon', name: 'Add New Coupon', component: AddCoupon },
  { path: '/coupons/coupon-list', name: 'Coupon List', component: CouponList },
  { path: '/coupons/edit-coupon', name: 'Edit Coupon', component: EditCoupon },
  { path: '/casts', name: 'Casts', component: CastList, exact: true },
  { path: '/casts/cast-detail', name: 'Cast Detail', component: CastDetail },
  { path: '/casts/add-cast', name: 'Add New Cast', component: AddCast },
  { path: '/casts/edit-cast', name: 'Edit Cast', component: EditCast },
  { path: '/casts/cast-list', name: 'Cast List', component: CastList },
  { path: '/languages', name: 'Languages', component: LanguageList, exact: true },
  { path: '/languages/add-language', name: 'Add New Language', component: AddLanguage },
  { path: '/languages/edit-language', name: 'Edit Language', component: EditLanguage },
  { path: '/languages/language-list', name: 'Language List', component: LanguageList },
  { path: '/profile', name: 'Profile', component: Profile },
]

export default routes
