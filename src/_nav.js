import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilNotes,
  cilSpeedometer,
  cilMovie,
  cilUser,
  cilGift,
  cilList,
  cilLibraryAdd,
  cilAccountLogout,
  cilSchool,
  cilLanguage,
  cilPeople,
  cilStar,
} from '@coreui/icons'
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: 'info',
    },
  },
  {
    component: CNavTitle,
    name: 'Components',
  },
  {
    component: CNavGroup,
    name: 'Movies',
    to: '/movies',
    icon: <CIcon icon={cilMovie} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'All Movies',
        icon: <CIcon icon={cilList} customClassName="nav-icon" />,
        to: '/movies/movie-list',
      },
      {
        component: CNavItem,
        name: 'Add New Movie  ',
        icon: <CIcon icon={cilLibraryAdd} customClassName="nav-icon" />,
        to: '/movies/add-movie',
      },
    ],
  },
  {
    component: CNavGroup,
    name: 'Users',
    to: '/users',
    icon: <CIcon icon={cilPeople} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'User List',
        icon: <CIcon icon={cilList} customClassName="nav-icon" />,
        to: '/users/user-list',
      },
    ],
  },
  {
    component: CNavGroup,
    name: 'Categories',
    icon: <CIcon icon={cilNotes} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Category List',
        icon: <CIcon icon={cilList} customClassName="nav-icon" />,
        to: '/category/category-list',
      },
      {
        component: CNavItem,
        name: 'Add New Category',
        icon: <CIcon icon={cilLibraryAdd} customClassName="nav-icon" />,
        to: '/category/add-category',
      },
    ],
  },
  {
    component: CNavGroup,
    name: 'Coupons',
    icon: <CIcon icon={cilGift} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Coupon List',
        icon: <CIcon icon={cilList} customClassName="nav-icon" />,
        to: '/coupons/coupon-list',
        badge: {
          color: 'success',
        },
      },
      {
        component: CNavItem,
        name: 'Add Coupon',
        icon: <CIcon icon={cilLibraryAdd} customClassName="nav-icon" />,
        to: '/coupons/add-coupon',
        badge: {
          color: 'success',
        },
      },
    ],
  },
  {
    component: CNavGroup,
    name: 'Casts',
    icon: <CIcon icon={cilUser} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Cast List',
        icon: <CIcon icon={cilList} customClassName="nav-icon" />,
        to: '/casts/cast-list',
        badge: {
          color: 'success',
        },
      },
      {
        component: CNavItem,
        name: 'Add Cast',
        icon: <CIcon icon={cilLibraryAdd} customClassName="nav-icon" />,
        to: '/casts/add-cast',
        badge: {
          color: 'success',
        },
      },
    ],
  },
  {
    component: CNavGroup,
    name: 'Languages',
    icon: <CIcon icon={cilLanguage} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Language List',
        icon: <CIcon icon={cilList} customClassName="nav-icon" />,
        to: '/languages/language-list',
        badge: {
          color: 'success',
        },
      },
      {
        component: CNavItem,
        name: 'Add Language',
        icon: <CIcon icon={cilLibraryAdd} customClassName="nav-icon" />,
        to: '/languages/add-language',
        badge: {
          color: 'success',
        },
      },
    ],
  },
  {
    component: CNavItem,
    name: 'Reviews',
    to: '/reviews/review-list',
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Profile',
    to: '/profile',
    icon: <CIcon icon={cilSchool} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Logout',
    to: '/login',
    icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />,
  },
]

export default _nav
