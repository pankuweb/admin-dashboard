import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CForm,
  CFormLabel,
  CFormInput,
  CFormTextarea,
  CFormFeedback,
  CButton,
  CFormSelect
} from '@coreui/react'
import Multiselect from 'multiselect-react-dropdown'
const AddMovie = () => {
  const [validated, setValidated] = React.useState(false)
  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    setValidated(true)
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Add New Movie</strong>
          </CCardHeader>
          <CCardBody>
            <CForm
              className="row g-3 needs-validation"
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
            >
              <CCol md={12}>
                <CFormLabel htmlFor="validationCustom01">Movie Title</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom02">Upload Movie</CFormLabel>
                <CFormInput type="file" id="formFileDisabled" />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Upload Trailer</CFormLabel>
                <CFormInput type="file" id="formFileDisabled" />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Time Duration</CFormLabel>
                <CFormInput
                  id="appt-time"
                  type="time"
                  name="appt-time"
                  step="2"
                  class="form-control"
                  defaultValue=""
                  required
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom04">Select Category</CFormLabel>
                {/* <CMultiSelect options={options} visible={false} /> */}
                {/* <CFormSelect id="validationCustom04">
                  <option>Thriller</option>
                  <option>Romance</option>
                </CFormSelect> */}
                <Multiselect
                  isObject={false}
                  onRemove={function noRefCheck() {}}
                  onSearch={function noRefCheck() {}}
                  onSelect={function noRefCheck() {}}
                  options={['Thriller', 'Romance', 'Action', 'Horror', 'Family']}
                />
                <CFormFeedback invalid>Please provide a valid city.</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Upload Subtitle</CFormLabel>
                <CFormInput type="file" id="formFileDisabled" />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="formFileDisabled">Movie Banner</CFormLabel>
                <CFormInput type="file" id="formFileDisabled" multiple/>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Select Language</CFormLabel>
                <Multiselect
                  isObject={false}
                  onRemove={function noRefCheck() {}}
                  onSearch={function noRefCheck() {}}
                  onSelect={function noRefCheck() {}}
                  options={['Hindi', 'English', 'Tamil', 'Urdu', 'Bengali']}
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Dhakad Rating</CFormLabel>
                <CFormSelect aria-label="Default select example">
                  <option>Select</option>
                  <option value="1">Rocket</option>
                  <option value="2">Sparkle</option>
                  <option value="3">Bomb</option>
                </CFormSelect>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Age Group</CFormLabel>
                <Multiselect
                  isObject={false}
                  onRemove={function noRefCheck() {}}
                  onSearch={function noRefCheck() {}}
                  onSelect={function noRefCheck() {}}
                  options={['G', 'PG', 'PG-13', 'R', 'NC-17']}
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Movie Casts</CFormLabel>
                <Multiselect
                  isObject={false}
                  onRemove={function noRefCheck() {}}
                  onSearch={function noRefCheck() {}}
                  onSelect={function noRefCheck() {}}
                  options={['Aman', 'Routash', 'Jonas', 'Parmod', 'Ramesh']}
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Price</CFormLabel>
                <CFormInput type="number" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Offer Price</CFormLabel>
                <CFormInput type="number" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={12}>
                <CFormLabel htmlFor="formFileDisabled">Movie Description</CFormLabel>
                <CFormTextarea
                  rows="5"
                  type="text"
                  id="validationCustom01"
                  defaultValue=""
                  required
                />
              </CCol>
              <CCol xs={12}>
                <CButton color="dark-light" type="submit">
                  Add Movie
                </CButton>
              </CCol>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default AddMovie
