import React from 'react'
import { NavLink } from 'react-router-dom'
import { CBadge, CRow, CCol, CCard, CNavLink } from '@coreui/react'
import { CSmartTable } from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import { cilTrash, cilPencil, cilNoteAdd } from '@coreui/icons'

const MovieList = () => {
  const moviesColumns = [
    {
      key: 'title',
      _style: { width: '40%' },
      _props: { className: 'fw-semibold' },
    },
    'duration',
    'languages',
    'category',
    { key: 'status', _style: { width: '20%' } },
    {
      key: 'show_details',
      label: '',
      _style: { width: '1%' },
      filter: false,
      sorter: false,
      _props: { className: 'fw-semibold' },
    },
  ]
  const moviesData = [
    {
      id: 1,
      title: 'Quintin Ed',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Drama',
      status: 'Active',
    },
    {
      id: 2,
      title: 'Enéas Kwadwo',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Active',
    },
    {
      id: 3,
      title: 'Agapetus Tadeáš',
      duration: '01:40:30',
      languages: 'Hindi',
      category: 'Romance',
      status: 'Inactive',
    },
    {
      id: 4,
      title: 'Carwyn Fachtna',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Romance',
      status: 'Active',
    },
    {
      id: 5,
      title: 'Nehemiah Tatius',
      duration: '01:40:30',
      languages: 'English',
      category: 'Drama',
      status: 'Active',
    },
    {
      id: 6,
      title: 'Ebbe Gemariah',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Inactive',
    },
    {
      id: 7,
      title: 'Ebbe Gemariah',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Active',
    },
    {
      id: 8,
      title: 'Carwyn Gemariah',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Active',
    },
    {
      id: 9,
      title: 'Ebbe Gemariah',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Active',
    },
    {
      id: 10,
      title: 'Carwyn Fachtna',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Active',
    },
    {
      id: 11,
      title: 'Carwyn Fachtna',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Active',
    },
    {
      id: 12,
      title: 'Agapetus Tadeáš',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Inactive',
    },
  ]
  const getBadge = (status) => {
    switch (status) {
      case 'Active':
        return 'success'
      case 'Inactive':
        return 'secondary'
      case 'Pending':
        return 'warning'
      case 'Banned':
        return 'danger'
      default:
        return 'primary'
    }
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="p-3 list">
          <CSmartTable
            activePage={1}
            cleaner
            clickableRows
            columns={moviesColumns}
            columnSorter
            items={moviesData}
            itemsPerPageSelect
            itemsPerPage={10}
            pagination
            scopedColumns={{
              status: (item) => (
                <td>
                  <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
                </td>
              ),
              show_details: (item) => {
                return (
                  <td className="py-2 d-flex">
                    <CNavLink className="p-0" to="/movies/movie-detail" component={NavLink}>
                      <CBadge color="dark">
                        <CIcon icon={cilNoteAdd} customClassName="nav-icon " />
                      </CBadge>
                    </CNavLink>
                    <CNavLink className="p-0 mx-1" to="/movies/edit-movie" component={NavLink}>
                      <CBadge color="success">
                        <CIcon icon={cilPencil} customClassName="nav-icon " />
                      </CBadge>
                    </CNavLink>
                    <CNavLink className="p-0" to="/" component={NavLink}>
                      <CBadge color="danger">
                        <CIcon icon={cilTrash} customClassName="nav-icon" />
                      </CBadge>
                    </CNavLink>
                  </td>
                )
              },
            }}
            sorterValue={{ column: 'name', state: 'asc' }}
            tableFilter
            tableHeadProps={{
              color: 'dark',
            }}
            tableProps={{
              striped: true,
              hover: true,
            }}
          />
        </CCard>
      </CCol>
    </CRow>
  )
}

export default MovieList
