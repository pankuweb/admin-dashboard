import React, { useState } from 'react'
import { CCard, CCardBody, CCardHeader, CCol, CRow, CFormLabel } from '@coreui/react'
import banner from './../../../assets/images/banner.jpg'
import Rating from 'src/components/Rating'

const initialState = { alt: '', src: banner }

const MovieDetail = () => {
  const [{ alt, src }] = useState(initialState)

  return (
    <CRow>
      <CCol md={4}>
        <CCard className="mb-4 banner-card">
          <CCardHeader className="text-center">
            <strong>Banner 1</strong>
          </CCardHeader>
          <CCardBody className="text-center">
            <CCol>
              <img className="card-img-top" src={src} alt="" />
            </CCol>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol md={4}>
        <CCard className="mb-4 banner-card">
          <CCardHeader className="text-center">
            <strong>Banner 2</strong>
          </CCardHeader>
          <CCardBody className="text-center">
            <CCol>
              <img className="preview" src={src} alt={alt} />
            </CCol>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol md={4}>
        <CCard className="mb-4 banner-card">
          <CCardHeader className="text-center">
            <strong>Banner 2</strong>
          </CCardHeader>
          <CCardBody className="text-center">
            <CCol>
              <img className="preview" src={src} alt={alt} />
            </CCol>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardBody>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <h2>Night Watch</h2>
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Time:</strong> 01:20:30
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Category:</strong> Action
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Age Group:</strong> PG
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Languages:</strong> Hindi, English
              </CFormLabel>
            </CCol>
            <hr />
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Dhakad Rating:</strong> Sparkle
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Casts:</strong> Jonas Chalia, Parmod Patwari, Aman Power
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Coupons:</strong> Coupon 10
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Price:</strong> 400
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Offter Price:</strong> 320
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Status:</strong> Active
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01" className="d-flex">
                <strong>Average Rating:</strong> <Rating />
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Total Users Rented This Movie:</strong> 245
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Description:</strong> During the height of the Cold War, two Russian
                astronauts prepare to step into the unknown during the first space walk.
              </CFormLabel>
            </CCol>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default MovieDetail
