import React, { useState } from 'react'
import {
  CAvatar,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CForm,
  CFormLabel,
  CFormInput,
  CFormFeedback,
  CFormSelect,
  CButton,
} from '@coreui/react'
import avatar6 from './../../assets/images/avatars/6.jpg'

const initialState = { alt: '', src: avatar6 }

const Profile = () => {
  const [validated, setValidated] = React.useState(false)
  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    setValidated(true)
  }

  const [{ alt, src }, setPreview] = useState(initialState)

  const fileHandler = (event) => {
    const { files } = event.target
    setPreview(
      files.length
        ? {
            src: URL.createObjectURL(files[0]),
            alt: files[0].name,
          }
        : initialState,
    )
  }

  //   const loadFile = (event) => {
  //     var image = document.getElementById('output')
  //     image.src = URL.createObjectURL(event.target.files[0])
  //   }
  return (
    <CRow>
      <CCol md={4}>
        <CCard className="mb-4 profile-card">
          <CCardHeader className="text-center">
            <strong>Update Profile Picture</strong>
          </CCardHeader>
          <CCardBody className="text-center">
            <CAvatar className="preview" src={src} alt={alt} />
            <div className="profile-input">
              Choose Image
              <input className="hide_file" accept="image/*" type="file" onChange={fileHandler} />
            </div>
          </CCardBody>
        </CCard>

        <CCard className="mb-4 profile-card">
          <CCardBody>
            <CForm
              className="row g-3 needs-validation"
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
            >
              <CCol md={12}>
                <CFormLabel htmlFor="validationCustom01">Password</CFormLabel>
                <CFormInput type="password" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={12}>
                <CFormLabel htmlFor="validationCustom01">Confirm Password</CFormLabel>
                <CFormInput type="password" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol xs={12}>
                <CButton color="dark-light" type="submit">
                  Update Password
                </CButton>
              </CCol>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol md={8}>
        <CCard className="mb-4">
          <CCardHeader className="text-center">
            <strong>Persnol Details</strong>
          </CCardHeader>
          <CCardBody>
            <CForm
              className="row g-3 needs-validation"
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
            >
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">First Name</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom02">Last Name</CFormLabel>
                <CFormInput type="text" id="validationCustom02" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Email</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Phone</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom04">Select Gender</CFormLabel>
                <CFormSelect id="validationCustom04">
                  <option>Choose...</option>
                  <option>Male</option>
                  <option>Female</option>
                  <option>Other</option>
                </CFormSelect>
                <CFormFeedback invalid>Please provide a valid city.</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Date Of Birth</CFormLabel>
                <CFormInput type="date" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">City</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">State</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Zip Code</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom04">Select Status</CFormLabel>
                <CFormSelect id="validationCustom04">
                  <option>Active</option>
                  <option>Inactive</option>
                </CFormSelect>
                <CFormFeedback invalid>Please provide a valid city.</CFormFeedback>
              </CCol>
              <CCol md={12}>
                <CFormLabel htmlFor="validationCustom01">Address</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol xs={12}>
                <CButton color="dark-light" type="submit">
                  Update details
                </CButton>
              </CCol>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Profile
