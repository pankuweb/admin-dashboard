import React, { lazy } from 'react'
import { CBadge, CCard } from '@coreui/react'
import { CSmartTable } from '@coreui/react-pro'

const WidgetsDropdown = lazy(() => import('../widgets/WidgetsDropdown.js'))

const Dashboard = () => {
  const users = [
    {
      key: 'name',
      _style: { width: '40%' },
      _props: { className: 'fw-semibold' },
    },
    'registered',
    { key: 'role', filter: false, sorter: false, _style: { width: '20%' } },
    { key: 'status', _style: { width: '20%' } },
  ]
  const usersData = [
    { id: 1, name: 'Quintin Ed', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 2, name: 'Enéas Kwadwo', registered: '2018/03/01', role: 'Member', status: 'Active' },
    { id: 3, name: 'Agapetus Tadeáš', registered: '2018/01/21', role: 'Member', status: 'Active' },
    { id: 4, name: 'Carwyn Fachtna', registered: '2018/01/01', role: 'Member', status: 'Active' },
    { id: 5, name: 'Nehemiah Tatius', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 6, name: 'Ebbe Gemariah', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 7, name: 'Ebbe Gemariah', registered: '2018/02/01', role: 'Member', status: 'Inactive' },
    { id: 8, name: 'Ebbe Gemariah', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 9, name: 'Ebbe Gemariah', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 10, name: 'Ebbe Gemariah', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 11, name: 'Agapetus Tadeáš', registered: '2018/01/21', role: 'Member', status: 'Active' },
  ]
  const moviesColumns = [
    {
      key: 'title',
      _style: { width: '40%' },
      _props: { className: 'fw-semibold' },
    },
    'duration',
    'languages',
    'category',
    { key: 'status', _style: { width: '20%' } },
  ]
  const moviesData = [
    {
      id: 1,
      title: 'Quintin Ed',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Drama',
      status: 'Active',
    },
    {
      id: 2,
      title: 'Enéas Kwadwo',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Inactive',
    },
    {
      id: 3,
      title: 'Agapetus Tadeáš',
      duration: '01:40:30',
      languages: 'Hindi',
      category: 'Romance',
      status: 'Active',
    },
    {
      id: 4,
      title: 'Carwyn Fachtna',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Romance',
      status: 'Active',
    },
    {
      id: 5,
      title: 'Nehemiah Tatius',
      duration: '01:40:30',
      languages: 'English',
      category: 'Drama',
      status: 'Active',
    },
    {
      id: 6,
      title: 'Ebbe Gemariah',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Inactive',
    },
    {
      id: 7,
      title: 'Ebbe Gemariah',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Inactive',
    },
    {
      id: 8,
      title: 'Carwyn Gemariah',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Inactive',
    },
    {
      id: 9,
      title: 'Ebbe Gemariah',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Inactive',
    },
    {
      id: 10,
      title: 'Carwyn Fachtna',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Inactive',
    },
    {
      id: 11,
      title: 'Carwyn Fachtna',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Inactive',
    },
    {
      id: 12,
      title: 'Agapetus Tadeáš',
      duration: '01:40:30',
      languages: 'Hindi,English',
      category: 'Thriller',
      status: 'Inactive',
    },
  ]
  const getBadge = (status) => {
    switch (status) {
      case 'Active':
        return 'success'
      case 'Inactive':
        return 'secondary'
      case 'Pending':
        return 'warning'
      case 'Banned':
        return 'danger'
      default:
        return 'primary'
    }
  }

  return (
    <>
      <WidgetsDropdown />
      <CCard className="p-3 mb-4 list">
        <h5 className="py-2">Recent Users</h5>
        <CSmartTable
          activePage={1}
          cleaner
          clickableRows
          columns={users}
          items={usersData}
          itemsPerPageSelect
          itemsPerPage={5}
          pagination
          scopedColumns={{
            status: (item) => (
              <td>
                <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
              </td>
            ),
          }}
          sorterValue={{ column: 'name', state: 'asc' }}
          tableFilter
          tableHeadProps={{
            color: 'dark',
          }}
          tableProps={{
            striped: true,
            hover: true,
          }}
        />
      </CCard>
      <CCard className="p-3 list">
        <h5 className="py-2">Recent Movies</h5>
        <CSmartTable
          activePage={1}
          cleaner
          clickableRows
          columns={moviesColumns}
          items={moviesData}
          itemsPerPageSelect
          itemsPerPage={5}
          pagination
          scopedColumns={{
            status: (item) => (
              <td>
                <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
              </td>
            ),
          }}
          sorterValue={{ column: 'name', state: 'asc' }}
          tableFilter
          tableHeadProps={{
            color: 'dark',
          }}
          tableProps={{
            striped: true,
            hover: true,
          }}
        />
      </CCard>
    </>
  )
}

export default Dashboard
