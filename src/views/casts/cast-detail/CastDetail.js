import React, { useState } from 'react'
import { CAvatar, CCard, CCardBody, CCardHeader, CCol, CRow, CFormLabel } from '@coreui/react'
import avatar6 from './../../../assets/images/avatars/6.jpg'

const initialState = { alt: '', src: avatar6 }

const CastDetail = () => {
  const [{ alt, src }] = useState(initialState)

  return (
    <CRow>
      <CCol md={4}>
        <CCard className="mb-4 profile-card">
          <CCardHeader className="text-center">
            <strong>Cast Picture</strong>
          </CCardHeader>
          <CCardBody className="text-center">
            <CCol>
              <CAvatar className="preview" src={src} alt={alt} />
            </CCol>
            <CCol className="mt-3 mb-1">
              <h3 className="m-0">Jonas Chalia</h3>
            </CCol>
            <CCol>
              <p>jonas@gmail.com</p>
            </CCol>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol md={8}>
        <CCard className="mb-4">
          <CCardHeader className="text-center">
            <strong>Cast Details</strong>
          </CCardHeader>
          <CCardBody>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Phone:</strong> +919001360432
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Gender:</strong> Male
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Date Of Birth:</strong> 05/12/2001
              </CFormLabel>
            </CCol>
            <hr />
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>City:</strong> Abohar
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>State:</strong> Punjab
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Zip Code:</strong> 335562
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Status:</strong> Active
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Address:</strong> Ward No 1, Axix street near bank
              </CFormLabel>
            </CCol>
            <hr />
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                Indrajith Sukumaran is an Indian film actor best known for his work in the Malayalam
                film industry. He is the son of the late actor Sukumaran and elder brother of
                Prithviraj. Indrajith made his debut in the film Oomappenninu Uriyadappayyan (2002)
                as one of the lead actors and followed it up with movies like Meesa Madhvan (2002),
                Runway (2004).
              </CFormLabel>
            </CCol>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CastDetail
