// import { cilTrash, cilEyedropper } from '@coreui/icons'
// import CIcon from '@coreui/icons-react'
import avatar8 from './../../../assets/images/avatars/3.jpg'
import avatar6 from './../../../assets/images/avatars/6.jpg'
import avatar4 from './../../../assets/images/avatars/4.jpg'

import React from 'react'
import { NavLink } from 'react-router-dom'
import CIcon from '@coreui/icons-react'
import { cilTrash, cilPencil, cilNoteAdd } from '@coreui/icons'

import { CNavLink, CBadge, CRow, CCol, CCard, CAvatar } from '@coreui/react'
import { CSmartTable } from '@coreui/react-pro'

const CastList = () => {
  const columns = [
    {
      key: 'image',
      _style: { width: '20%' },
    },
    {
      key: 'name',
      _style: { width: '40%' },
      _props: { className: 'fw-semibold' },
    },
    { key: 'status', _style: { width: '20%' } },
    {
      key: 'show_details',
      label: '',
      _style: { width: '1%' },
      filter: false,
      sorter: false,
      _props: { className: 'fw-semibold' },
    },
  ]
  const usersData = [
    {
      img: avatar8,
      name: 'Quintin Ed',

      category: 'Drama',
      status: 'Active',
    },
    {
      img: avatar6,
      name: 'Enéas Kwadwo',

      category: 'Thriller',
      status: 'Active',
    },
    {
      img: avatar4,
      name: 'Agapetus Tadeáš',
      ategory: 'Romance',
      status: 'Active',
    },
    {
      img: avatar6,
      name: 'Carwyn Fachtna',

      category: 'Romance',
      status: 'Active',
    },
    {
      img: avatar4,
      name: 'Nehemiah Tatius',
      category: 'Drama',
      status: 'Active',
    },
    {
      img: avatar8,
      name: 'Ebbe Gemariah',

      category: 'Thriller',
      status: 'Active',
    },
    {
      img: avatar6,
      name: 'Ebbe Gemariah',

      category: 'Thriller',
      status: 'Active',
    },
    {
      img: avatar8,
      name: 'Carwyn Gemariah',

      category: 'Thriller',
      status: 'Active',
    },
    {
      img: avatar8,
      name: 'Ebbe Gemariah',

      category: 'Thriller',
      status: 'Active',
    },
    {
      img: avatar6,
      name: 'Carwyn Fachtna',

      category: 'Thriller',
      status: 'Active',
    },
    {
      img: avatar4,
      name: 'Carwyn Fachtna',

      category: 'Thriller',
      status: 'Active',
    },
    {
      img: avatar8,
      name: 'Agapetus Tadeáš',

      category: 'Thriller',
      status: 'Active',
    },
  ]
  const getBadge = (status) => {
    switch (status) {
      case 'Active':
        return 'success'
      case 'Inactive':
        return 'secondary'
      case 'Pending':
        return 'warning'
      case 'Banned':
        return 'danger'
      default:
        return 'primary'
    }
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="p-3 list">
          <CSmartTable
            activePage={1}
            cleaner
            clickableRows
            columns={columns}
            columnSorter
            items={usersData}
            itemsPerPageSelect
            itemsPerPage={10}
            pagination
            scopedColumns={{
              status: (item) => (
                <td>
                  <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
                </td>
              ),
              show_details: (item) => {
                return (
                  <td className="py-2 d-flex">
                    <CNavLink className="px-0" to="/casts/cast-detail" component={NavLink}>
                      <CBadge color="dark">
                        <CIcon icon={cilNoteAdd} customClassName="nav-icon " />
                      </CBadge>
                    </CNavLink>
                    <CNavLink className="px-0 mx-1" to="/casts/edit-cast" component={NavLink}>
                      <CBadge color="success">
                        <CIcon icon={cilPencil} customClassName="nav-icon " />
                      </CBadge>
                    </CNavLink>
                    <CNavLink className="px-0" to="/" component={NavLink}>
                      <CBadge color="danger">
                        <CIcon icon={cilTrash} customClassName="nav-icon" />
                      </CBadge>
                    </CNavLink>
                  </td>
                )
              },
              image: (usersData) => {
                return (
                  <td className="py-2">
                    <CAvatar src={usersData.img} size="md" />
                  </td>
                )
              },
            }}
            sorterValue={{ column: 'name', state: 'asc' }}
            tableFilter
            tableHeadProps={{
              color: 'dark',
            }}
            tableProps={{
              striped: true,
              hover: true,
            }}
          />
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CastList
