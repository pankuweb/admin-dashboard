import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CForm,
  CFormLabel,
  CFormInput,
  CFormFeedback,
  CFormSelect,
  CButton,
  CFormTextarea,
} from '@coreui/react'

const EditCast = () => {
  const [validated, setValidated] = React.useState(false)
  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    setValidated(true)
  }
  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Edit Cast</strong>
          </CCardHeader>
          <CCardBody>
            <CForm
              className="row g-3 needs-validation"
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
            >
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">First Name</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="John" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom02">Last Name</CFormLabel>
                <CFormInput type="text" id="validationCustom02" defaultValue="Doe" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom04">Select Gender</CFormLabel>
                <CFormSelect id="validationCustom04">
                  <option>Choose...</option>
                  <option selected>Male</option>
                  <option>Female</option>
                  <option>Other</option>
                </CFormSelect>
                <CFormFeedback invalid>Please provide a valid city.</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Date Of Birth</CFormLabel>
                <CFormInput
                  type="date"
                  id="validationCustom01"
                  defaultValue="02/10/2001"
                  required
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">City</CFormLabel>
                <CFormInput
                  type="text"
                  id="validationCustom01"
                  defaultValue="SadulShahr"
                  required
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">State</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="Punjab" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom04">Select Status</CFormLabel>
                <CFormSelect id="validationCustom04">
                  <option>Active</option>
                  <option selected>Inactive</option>
                </CFormSelect>
                <CFormFeedback invalid>Please provide a valid city.</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="formFileDisabled">Cast Image</CFormLabel>
                <CFormInput type="file" id="formFileDisabled" />
              </CCol>
              <CCol md={12}>
                <CFormLabel htmlFor="validationCustom01">Address</CFormLabel>
                <CFormInput
                  type="text"
                  id="validationCustom01"
                  defaultValue="will street hohn area"
                  required
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={12}>
                <CFormLabel htmlFor="validationCustom01">Description</CFormLabel>
                <CFormTextarea
                  rows="1"
                  type="text"
                  id="validationCustom01"
                  defaultValue="Indrajith Sukumaran is an Indian film actor best known for his work in the Malayalam film industry. "
                  required
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol xs={12}>
                <CButton color="dark-light" type="submit">
                  Update Cast
                </CButton>
              </CCol>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default EditCast
