import React from 'react'
import { NavLink } from 'react-router-dom'
import CIcon from '@coreui/icons-react'
import { cilTrash, cilPencil } from '@coreui/icons'

import { CNavLink, CBadge, CRow, CCol, CCard } from '@coreui/react'
import { CSmartTable } from '@coreui/react-pro'

const CategoryList = () => {
  const categoryColumns = [
    {
      key: 'title',
      _style: { width: '40%' },
      _props: { className: 'fw-semibold' },
    },
    'registered',
    { key: 'status', _style: { width: '20%' } },
    {
      key: 'show_details',
      label: '',
      _style: { width: '1%' },
      filter: false,
      sorter: false,
      _props: { className: 'fw-semibold' },
    },
  ]
  const categoryData = [
    { id: 1, title: 'Thriller', registered: '2018/02/01', status: 'Inactive' },
    { id: 2, title: 'Drama', registered: '2018/03/01', status: 'Active' },
    { id: 3, title: 'Romance', registered: '2018/01/21', status: 'Active' },
    { id: 4, title: 'Action', registered: '2018/01/01', status: 'Active' },
    { id: 5, title: 'Animation', registered: '2018/02/01', status: 'Active' },
    { id: 6, title: 'Comedy', registered: '2018/02/01', status: 'Inactive' },
    { id: 7, title: 'Crime', registered: '2018/02/01', status: 'Inactive' },
    { id: 8, title: 'Family', registered: '2018/02/01', status: 'Inactive' },
    { id: 9, title: 'Horror', registered: '2018/02/01', status: 'Inactive' },
    { id: 10, title: 'Mystery', registered: '2018/02/01', status: 'Inactive' },
    { id: 11, title: 'Fantasy', registered: '2018/01/21', status: 'Active' },
  ]
  const getBadge = (status) => {
    switch (status) {
      case 'Active':
        return 'success'
      case 'Inactive':
        return 'secondary'
      case 'Pending':
        return 'warning'
      case 'Banned':
        return 'danger'
      default:
        return 'primary'
    }
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="p-3 list">
          <CSmartTable
            activePage={1}
            cleaner
            clickableRows
            columns={categoryColumns}
            columnSorter
            items={categoryData}
            itemsPerPageSelect
            itemsPerPage={10}
            pagination
            scopedColumns={{
              status: (item) => (
                <td>
                  <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
                </td>
              ),
              show_details: (item) => {
                return (
                  <td className="py-2 d-flex">
                    <CNavLink className="p-0 mx-1" to="/category/edit-category" component={NavLink}>
                      <CBadge color="success">
                        <CIcon icon={cilPencil} customClassName="nav-icon " />
                      </CBadge>
                    </CNavLink>
                    <CNavLink className="p-0" to="/" component={NavLink}>
                      <CBadge color="danger">
                        <CIcon icon={cilTrash} customClassName="nav-icon" />
                      </CBadge>
                    </CNavLink>
                  </td>
                )
              },
            }}
            sorterValue={{ column: 'name', state: 'asc' }}
            tableFilter
            tableHeadProps={{
              color: 'dark',
            }}
            tableProps={{
              striped: true,
              hover: true,
            }}
          />
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CategoryList
