import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CForm,
  CFormLabel,
  CFormInput,
  CFormFeedback,
  CFormSelect,
  CButton,
} from '@coreui/react'

const EditCategory = () => {
  const [validated, setValidated] = React.useState(false)
  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    setValidated(true)
  }
  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Edit Category</strong>
          </CCardHeader>
          <CCardBody>
            <CForm
              className="row g-3 needs-validation"
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
            >
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Category Title</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="Action" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom04">Category Status</CFormLabel>
                <CFormSelect id="validationCustom04">
                  <option selected>Active</option>
                  <option>Inactive</option>
                </CFormSelect>
                <CFormFeedback invalid>Please provide a valid Status.</CFormFeedback>
              </CCol>
              <CCol xs={12}>
                <CButton color="dark-light" type="submit">
                  Edit Category
                </CButton>
              </CCol>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default EditCategory
