import React from 'react'
import { NavLink } from 'react-router-dom'

import { CBadge, CNavLink, CRow, CCol, CCard } from '@coreui/react'
import { CSmartTable } from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import { cilTrash, cilPencil, cilNoteAdd } from '@coreui/icons'

const UserList = () => {
  const columns = [
    {
      key: 'name',
      _style: { width: '40%' },
      _props: { className: 'fw-semibold' },
    },
    'registered',
    { key: 'role', filter: false, sorter: false, _style: { width: '20%' } },
    { key: 'status', _style: { width: '20%' } },
    {
      key: 'show_details',
      label: '',
      _style: { width: '1%' },
      filter: false,
      sorter: false,
      _props: { className: 'fw-semibold' },
    },
  ]
  const usersData = [
    { id: 1, name: 'Quintin Ed', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 2, name: 'Enéas Kwadwo', registered: '2018/03/01', role: 'Member', status: 'Active' },
    { id: 3, name: 'Agapetus Tadeáš', registered: '2018/01/21', role: 'Member', status: 'Active' },
    { id: 4, name: 'Carwyn Fachtna', registered: '2018/01/01', role: 'Member', status: 'Active' },
    { id: 5, name: 'Nehemiah Tatius', registered: '2018/02/01', role: 'Member', status: 'Blocked' },
    { id: 6, name: 'Ebbe Gemariah', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 7, name: 'Ebbe Gemariah', registered: '2018/02/01', role: 'Member', status: 'Inactive' },
    { id: 8, name: 'Ebbe Gemariah', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 9, name: 'Ebbe Gemariah', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 10, name: 'Ebbe Gemariah', registered: '2018/02/01', role: 'Member', status: 'Active' },
    { id: 11, name: 'Agapetus Tadeáš', registered: '2018/01/21', role: 'Member', status: 'Active' },
  ]
  const getBadge = (status) => {
    switch (status) {
      case 'Active':
        return 'success'
      case 'Inactive':
        return 'secondary'
      case 'Pending':
        return 'warning'
      case 'Blocked':
        return 'danger'
      default:
        return 'primary'
    }
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="p-3 list">
          <CSmartTable
            activePage={1}
            cleaner
            clickableRows
            columns={columns}
            columnSorter
            items={usersData}
            itemsPerPageSelect
            itemsPerPage={10}
            pagination
            scopedColumns={{
              status: (item) => (
                <td>
                  <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
                </td>
              ),
              show_details: (item) => {
                return (
                  <td className="py-2 d-flex">
                    <CNavLink className="p-0" to="/users/user-detail" component={NavLink}>
                      <CBadge color="dark">
                        <CIcon icon={cilNoteAdd} customClassName="nav-icon " />
                      </CBadge>
                    </CNavLink>
                    <CNavLink className="p-0 mx-1" to="/users/edit-user" component={NavLink}>
                      <CBadge color="success">
                        <CIcon icon={cilPencil} customClassName="nav-icon " />
                      </CBadge>
                    </CNavLink>
                    <CNavLink className="p-0" to="/" component={NavLink}>
                      <CBadge color="danger">
                        <CIcon icon={cilTrash} customClassName="nav-icon" />
                      </CBadge>
                    </CNavLink>
                  </td>
                )
              },
            }}
            sorterValue={{ column: 'name', state: 'asc' }}
            tableFilter
            tableHeadProps={{
              color: 'dark',
            }}
            tableProps={{
              striped: true,
              hover: true,
            }}
          />
        </CCard>
      </CCol>
    </CRow>
  )
}

export default UserList
