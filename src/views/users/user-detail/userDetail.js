import React, { useState } from 'react'
import { CAvatar, CCard, CCardBody, CCardHeader, CCol, CRow, CFormLabel } from '@coreui/react'
import avatar6 from './../../../assets/images/avatars/6.jpg'

const initialState = { alt: '', src: avatar6 }

const UserDetail = () => {
  const [{ alt, src }] = useState(initialState)

  return (
    <CRow>
      <CCol xs={4}>
        <CCard className="mb-4 profile-card">
          <CCardHeader className="text-center">
            <strong>Profile Picture</strong>
          </CCardHeader>
          <CCardBody className="text-center">
            <CCol>
              <CAvatar className="preview" src={src} alt={alt} />
            </CCol>
            <CCol className="mt-3 mb-1">
              <h3 className="m-0">Jonas Chalia</h3>
            </CCol>
            <CCol>
              <p>jonas@gmail.com</p>
            </CCol>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol xs={8}>
        <CCard className="mb-4">
          <CCardHeader className="text-center">
            <strong>More Details</strong>
          </CCardHeader>
          <CCardBody>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Phone:</strong> +919001360432
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Gender:</strong> Male
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Date Of Birth:</strong> 05/12/2001
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>City:</strong> Abohar
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>State:</strong> Punjab
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Zip Code:</strong> 335562
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Status:</strong> Active
              </CFormLabel>
            </CCol>
            <CCol md={12}>
              <CFormLabel htmlFor="validationCustom01">
                <strong>Address:</strong> Ward No 1, Axix street near bank
              </CFormLabel>
            </CCol>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default UserDetail
