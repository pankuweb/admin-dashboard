import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CForm,
  CFormLabel,
  CFormInput,
  CFormFeedback,
  CFormSelect,
  CButton,
  CFormTextarea,
} from '@coreui/react'

const EditUser = () => {
  const [validated, setValidated] = React.useState(false)
  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    setValidated(true)
  }
  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Edit User</strong>
          </CCardHeader>
          <CCardBody>
            <CForm
              className="row g-3 needs-validation"
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
            >
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">First Name</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="John" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom02">Last Name</CFormLabel>
                <CFormInput type="text" id="validationCustom02" defaultValue="Doe" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Email</CFormLabel>
                <CFormInput
                  type="text"
                  id="validationCustom01"
                  defaultValue="johndoe@gmail.com"
                  required
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Phone No</CFormLabel>
                <CFormInput
                  type="text"
                  id="validationCustom01"
                  defaultValue="+919001360432"
                  required
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom04">Select Gender</CFormLabel>
                <CFormSelect id="validationCustom04">
                  <option>Choose...</option>
                  <option selected>Male</option>
                  <option>Female</option>
                  <option>Other</option>
                </CFormSelect>
                <CFormFeedback invalid>Please provide a valid city.</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Date Of Birth</CFormLabel>
                <CFormInput
                  type="date"
                  id="validationCustom01"
                  defaultValue="05/11/2001"
                  required
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">City</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="Abohar" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">State</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="Punjab" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Zip Code</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="335062" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom04">Select Status</CFormLabel>
                <CFormSelect id="validationCustom04">
                  <option>Active</option>
                  <option selected>Inactive</option>
                </CFormSelect>
                <CFormFeedback invalid>Please provide a valid city.</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="formFileDisabled">User Image</CFormLabel>
                <CFormInput type="file" id="formFileDisabled" />
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="formFileDisabled">Address</CFormLabel>
                <CFormTextarea
                  rows="1"
                  type="text"
                  id="validationCustom01"
                  defaultValue="Ward No 1, Jong doe street, willberg"
                  required
                />
              </CCol>
              <CCol xs={12}>
                <CButton color="dark-light" type="submit">
                  Edit User
                </CButton>
              </CCol>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default EditUser
