import CoreUIIcons from './add-coupon'
import Flags from './flags'
import Brands from './brands'

export { CoreUIIcons, Flags, Brands }
