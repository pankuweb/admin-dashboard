import React from 'react'
import { NavLink } from 'react-router-dom'
import CIcon from '@coreui/icons-react'
import { cilTrash, cilPencil } from '@coreui/icons'

import { CNavLink, CBadge, CRow, CCol, CCard } from '@coreui/react'
import { CSmartTable } from '@coreui/react-pro'

const CouponList = () => {
  const couponColumns = [
    {
      key: 'title',
      _style: { width: '40%' },
      _props: { className: 'fw-semibold' },
    },
    'amount',
    { key: 'status', _style: { width: '20%' } },
    {
      key: 'show_details',
      label: '',
      _style: { width: '1%' },
      filter: false,
      sorter: false,
      _props: { className: 'fw-semibold' },
    },
  ]
  const couponsData = [
    { id: 1, title: 'First', amount: '80', status: 'Active' },
    { id: 2, title: 'Second', amount: '70', status: 'Active' },
    { id: 3, title: 'Third', amount: '90', status: 'Active' },
    { id: 4, title: 'Forth', amount: '20', status: 'Active' },
    { id: 5, title: 'Fifth', amount: '80', status: 'Active' },
    { id: 6, title: 'Sixth', amount: '80', status: 'Inactive' },
    { id: 7, title: 'Seventh', amount: '80', status: 'Inactive' },
    { id: 8, title: 'Test', amount: '80', status: 'Inactive' },
    { id: 9, title: 'Test1', amount: '80', status: 'Inactive' },
    { id: 10, title: 'Test2', amount: '80', status: 'Inactive' },
    { id: 11, title: 'Test3', amount: '90', status: 'Active' },
  ]
  const getBadge = (status) => {
    switch (status) {
      case 'Active':
        return 'success'
      case 'Inactive':
        return 'secondary'
      case 'Pending':
        return 'warning'
      case 'Banned':
        return 'danger'
      default:
        return 'primary'
    }
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="p-3 list">
          <CSmartTable
            activePage={1}
            cleaner
            clickableRows
            columns={couponColumns}
            columnSorter
            items={couponsData}
            itemsPerPageSelect
            itemsPerPage={10}
            pagination
            scopedColumns={{
              status: (item) => (
                <td>
                  <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
                </td>
              ),
              show_details: (item) => {
                return (
                  <td className="py-2 d-flex">
                    <CNavLink className="p-0 mx-1" to="/coupons/edit-coupon" component={NavLink}>
                      <CBadge color="success">
                        <CIcon icon={cilPencil} customClassName="nav-icon " />
                      </CBadge>
                    </CNavLink>
                    <CNavLink className="p-0" to="/" component={NavLink}>
                      <CBadge color="danger">
                        <CIcon icon={cilTrash} customClassName="nav-icon" />
                      </CBadge>
                    </CNavLink>
                  </td>
                )
              },
            }}
            sorterValue={{ column: 'name', state: 'asc' }}
            tableFilter
            tableHeadProps={{
              color: 'dark',
            }}
            tableProps={{
              striped: true,
              hover: true,
            }}
          />
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CouponList
