import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CForm,
  CFormLabel,
  CFormInput,
  CFormFeedback,
  CFormSelect,
  CButton,
  CFormTextarea,
  CFormCheck
} from '@coreui/react'
import Multiselect from 'multiselect-react-dropdown'

const AddCoupon = () => {
  const [validated, setValidated] = React.useState(false)
  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    setValidated(true)
  }
  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4 add-coupon">
          <CCardHeader>
            <strong>Add New Coupon</strong>
          </CCardHeader>
          <CCardBody>
            <CForm
              className="row g-3 needs-validation"
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
            >
              <CCol md={6} className="coupon-code">
                <CFormLabel htmlFor="validationCustom01">Coupon Code</CFormLabel>
                <CFormInput type="text" id="validationCustom01" defaultValue="" required />
                <CButton className="generate-code" color="dark-light" size="sm" shape="rounded-pill">
                  Generate Code
                </CButton>
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Coupon Amount</CFormLabel>
                <CFormInput type="number" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="formFileDisabled">Description</CFormLabel>
                <CFormTextarea
                  rows="1"
                  type="text"
                  id="validationCustom01"
                  defaultValue=""
                  required
                />
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom04">Coupon Status</CFormLabel>
                <CFormSelect id="validationCustom04">
                  <option>Active</option>
                  <option>Inactive</option>
                </CFormSelect>
                <CFormFeedback invalid>Please provide a valid Status.</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Coupon Expire</CFormLabel>
                <CFormInput type="date" id="validationCustom01" defaultValue="" required />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={6}>
                <CFormLabel htmlFor="validationCustom01">Users Limit</CFormLabel>
                <CFormSelect aria-label="Default select example">
                  <option>Select</option>
                  <option value="1">One Per User</option>
                  <option value="2">Two Per User</option>
                </CFormSelect>
              </CCol>
              <CCol md={12}>
                <CFormLabel htmlFor="validationCustom04">Select Movies</CFormLabel>
                <Multiselect
                  isObject={false}
                  onRemove={function noRefCheck() {}}
                  onSearch={function noRefCheck() {}}
                  onSelect={function noRefCheck() {}}
                  options={['Ishqiya', 'Zindagi Na Milegi Dobara', 'Rockstar', 'Delhi Belly', 'Udaan ']}
                />
                <CFormFeedback invalid>Please provide a valid city.</CFormFeedback>
              </CCol>
              <CCol md={12}>
                <CFormCheck id="flexCheckDefault" label="Select All Movies"/>
              </CCol>
              <CCol xs={12}>
                <CButton color="dark-light" type="submit">
                  Add Coupon
                </CButton>
              </CCol>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default AddCoupon
