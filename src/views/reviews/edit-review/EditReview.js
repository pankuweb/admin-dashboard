import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CForm,
  CFormLabel,
  CFormInput,
  CFormFeedback,
  CButton,
  CFormTextarea,
} from '@coreui/react'

const EditReview = () => {
  const [validated, setValidated] = React.useState(false)
  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    setValidated(true)
  }
  return (
    <CRow>
      <CCol className="m-auto" xs={10}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Edit Review</strong>
          </CCardHeader>
          <CCardBody>
            <CForm
              className="row g-3 needs-validation"
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
            >
              <CCol md={12}>
                <CFormLabel htmlFor="validationCustom01">Review Title</CFormLabel>
                <CFormInput
                  type="text"
                  id="validationCustom01"
                  defaultValue="First Review"
                  required
                />
                <CFormFeedback valid>Looks good!</CFormFeedback>
              </CCol>
              <CCol md={12}>
                <CFormLabel htmlFor="formFileDisabled">Description</CFormLabel>
                <CFormTextarea
                  rows="4"
                  type="text"
                  id="validationCustom01"
                  defaultValue="This is testing detail"
                  required
                />
              </CCol>
              <CCol xs={12}>
                <CButton color="dark-light" type="submit">
                  Update Review
                </CButton>
              </CCol>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default EditReview
