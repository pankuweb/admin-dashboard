import React from 'react'
import { NavLink } from 'react-router-dom'
import CIcon from '@coreui/icons-react'
import { cilStar } from '@coreui/icons'
import { CNavLink, CBadge, CRow, CCol, CCard } from '@coreui/react'
import { CSmartTable } from '@coreui/react-pro'

const ReviewList = () => {
  const couponColumns = [
    {
      key: 'user',
      _style: { width: '25%' },
      _props: { className: 'fw-semibold' },
    },
    {
      key: 'review',
      _style: { width: '25%' },
      _props: { className: 'fw-semibold' },
    },
    'rating',
    {
      key: 'show_details',
      label: '',
      _style: { width: '20%' },
      filter: false,
      sorter: false,
      _props: { className: 'fw-semibold' },
    },
  ]
  const couponsData = [
    { id: 1, user: 'Did Damn', review: 'Nice Movie' },
    { id: 2, user: 'John Doe', review: 'Best Movie Forever' },
    { id: 3, user: 'Jonas sss', review: 'Nice Movie' },
    { id: 4, user: 'John Doe', review: 'Worst Movie' },
    { id: 5, user: 'John test', review: 'Nice Movie' },
    { id: 6, user: 'John Doe', review: 'Comedy Movie' },
    { id: 7, user: 'Rohan virk', review: 'Great!' },
    { id: 8, user: 'John Doe', review: 'Family' },
    { id: 9, user: 'John Doe', review: 'SUpbb' },
    { id: 10, user: 'Ammy Doe', review: 'Mystery' },
    { id: 11, user: 'John Doe', review: 'Fantasy' },
  ]
  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="p-3 list">
          <CSmartTable
            activePage={1}
            cleaner
            clickableRows
            columns={couponColumns}
            columnSorter
            items={couponsData}
            itemsPerPageSelect
            itemsPerPage={10}
            pagination
            scopedColumns={{
              rating: (item) => (
                <td>
                  <CIcon icon={cilStar} customClassName="nav-icon" />
                  <CIcon icon={cilStar} customClassName="nav-icon" />
                  <CIcon icon={cilStar} customClassName="nav-icon" />
                  <CIcon icon={cilStar} customClassName="nav-icon" />
                  <CIcon icon={cilStar} customClassName="nav-icon" />
                </td>
              ),
              show_details: (item) => {
                return (
                  <td className="py-2 d-flex">
                    <CNavLink className="p-0" to="/reviews/edit-review" component={NavLink}>
                      <CBadge color="success">Edit</CBadge>
                    </CNavLink>
                    <CNavLink className="p-0 mx-1" to="/" component={NavLink}>
                      <CBadge color="success">Approve</CBadge>
                    </CNavLink>
                    <CNavLink className="p-0 mx-1" to="/" component={NavLink}>
                      <CBadge color="danger">Reject</CBadge>
                    </CNavLink>
                    <CNavLink className="p-0" to="/" component={NavLink}>
                      <CBadge color="danger">Delete</CBadge>
                    </CNavLink>
                  </td>
                )
              },
            }}
            sorterValue={{ column: 'name', state: 'asc' }}
            tableFilter
            tableHeadProps={{
              color: 'dark',
            }}
            tableProps={{
              striped: true,
              hover: true,
            }}
          />
        </CCard>
      </CCol>
    </CRow>
  )
}

export default ReviewList
